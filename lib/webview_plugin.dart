import 'dart:async';

import 'package:flutter/services.dart';

class WebViewPlugin {
  static const MethodChannel _channel = const MethodChannel('webview_plugin');

  static Future<Null> launch(String url, Function callback) async {
    Map<String, dynamic> args = {
      "url": url,
    };
    final String result = await _channel.invokeMethod("loadUrl", args);

    if (callback != null) {
      callback(result);
    }
  }
}
