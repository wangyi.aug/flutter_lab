import 'dart:io';
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:flutter_sample/model/movie.dart';
import 'package:flutter_sample/widget/rating_bar.dart';

class MovieFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => MovieFragmentState();
}

class MovieFragmentState extends State<MovieFragment> {
  // 数据集合
  List<Movie> movieList = List();

  // 分页大小
  final int pageSize = 10;

  // 当前页
  int currentPage = 0;

  // 加载中
  bool loading = false;

  // ListView 滑动监听
  ScrollController scrollController = ScrollController();

  int selectedIndex = -1;

  @override
  void initState() {
    super.initState();
    scrollController.addListener(() {
      if (scrollController.position.pixels ==
          scrollController.position.maxScrollExtent) {
        if (!loading) {
          getMovieList();
        }
      }
    });
    getMovieList();
  }

  @override
  void dispose() {
    scrollController.dispose();
    super.dispose();
  }

  void getMovieList() async {
    loading = true;

    var httpClient = HttpClient();
    var uri = Uri.http('api.douban.com', '/v2/movie/top250', {
      'start': currentPage.toString(),
      'count': pageSize.toString(),
    });

    var request = await httpClient.getUrl(uri);
    var response = await request.close();

    if (response.statusCode == HttpStatus.ok) {
      String source = await response.transform(utf8.decoder).join();
      MovieListResponse resp = MovieListResponse.fromJson(json.decode(source));

      if (resp.count == 0) {
        // todo 加载完成
        loading = false;
        return;
      }

      setState(() {
        if (currentPage == 0 && movieList.length > 0) {
          movieList.clear();
        }
        movieList.addAll(resp.subjects);
        currentPage += resp.count;
      });
    } else {
      // todo 加载失败
    }

    loading = false;
  }

  // 下拉刷新
  Future<Null> refresh() async {
    currentPage = 0;
    getMovieList();
  }

  @override
  Widget build(BuildContext context) {
    return RefreshIndicator(
        child: ListView.builder(
          itemBuilder: buildItem,
          itemCount: movieList.length + 1,
          controller: scrollController,
        ),
        onRefresh: refresh);
  }

  Widget buildItem(BuildContext context, int index) {
    if (index == movieList.length) {
      return buildLoading(context);
    }

    Movie movie = movieList[index];
    List<String> genres = movie.genres;
    List<Director> directors = movie.directors;
    List<Cast> casts = movie.casts;

    StringBuffer genresText = StringBuffer();
    for (int i = 0; i < genres.length; i++) {
      if (i > 0) {
        genresText.write("/");
      }
      genresText.write(genres[i]);
    }

    StringBuffer directorsText = StringBuffer();
    for (int i = 0; i < directors.length; i++) {
      if (i > 0) {
        directorsText.write("/");
      }
      directorsText.write(directors[i].name);
    }

    StringBuffer castsText = StringBuffer();
    for (int i = 0; i < casts.length; i++) {
      if (i > 0) {
        castsText.write("/");
      }
      castsText.write(casts[i].name);
    }

    return GestureDetector(
      onTap: () {
//        var WebViewPlugin = WebViewPlugin();
//        WebViewPlugin.launch();
//        Navigator.pushNamed(context, '/movie/details');
      },
      onTapDown: (details) {
        setState(() {
          selectedIndex = index;
        });
      },
      onTapUp: (details) {
        setState(() {
          selectedIndex = -1;
        });
      },
      onTapCancel: () {
        setState(() {
          selectedIndex = -1;
        });
      },
      child: Card(
        color: index == selectedIndex ? Color(0XFFF5F5F5) : Colors.white,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.start,
          verticalDirection: VerticalDirection.down,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.all(8.0),
              child: Image.network(
                movie.images.small,
                width: 100.0,
                height: 148.0,
                fit: BoxFit.cover,
              ),
            ),
            Expanded(
                child: Padding(
              padding: EdgeInsets.all(8.0),
              child: Column(
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    "${index + 1}. ${movie.title}",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      color: Colors.blueAccent,
                      fontSize: 18.0,
                    ),
                  ),
                  Text(
                    "分类： ${genresText.toString()}",
                    style: TextStyle(
                      color: Color(0xFF333333),
                    ),
                  ),
                  Text(
                    "导演： ${directorsText.toString()}",
                    style: TextStyle(
                      color: Color(0xFF333333),
                    ),
                  ),
                  Text(
                    "主演： ${castsText.toString()}",
                    style: TextStyle(
                      color: Color(0xFF333333),
                    ),
                  ),
                  Text(
                    "年代： ${movie.year}",
                    style: TextStyle(
                      color: Color(0xFF333333),
                    ),
                  ),
                  Row(
                    children: <Widget>[
                      Text(
                        "评分： ",
                        style: TextStyle(
                          color: Color(0xFF333333),
                        ),
                      ),
                      StarRatingBar(
                        rating: movie.rating.average / 2,
                        starSize: 20.0,
                        color: Colors.amber,
                      ),
                      Padding(
                        padding: EdgeInsets.fromLTRB(8.0, 0.0, 0.0, 0.0),
                        child: Text(
                          movie.rating.average.toString(),
                          style: TextStyle(color: Color(0XFF666666)),
                        ),
                      )
                    ],
                  )
                ],
              ),
            )),
          ],
        ),
      ),
    );
  }

  Widget buildLoading(BuildContext context) {
    return Center(child: CircularProgressIndicator());
  }
}
