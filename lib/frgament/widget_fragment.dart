import 'package:flutter/material.dart';

class WidgetFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => WidgetFragmentState();
}

class WidgetFragmentState extends State<WidgetFragment> {
  Widget build(BuildContext context) {
    TextStyle titleStyle = TextStyle(
      color: Colors.blueAccent,
    );

    bool checkBox = true;
    int radio = 1;

    onCheckBoxChanged(bool value) {
      setState(() {
        checkBox = value;
      });
    }

    onRadioChanged(value) {
      setState(() {
        radio = value;
      });
    }

    return Scaffold(
      body: SingleChildScrollView(
          child: Padding(
        padding: EdgeInsets.all(16.0),
        child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            verticalDirection: VerticalDirection.down,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
                child: Text(
                  "TextField:",
                  style: titleStyle,
                ),
              ),
              TextField(
                  decoration: InputDecoration(
                hintText: 'Type something',
              )),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 0.0),
                child: Text(
                  "Button:",
                  style: titleStyle,
                ),
              ),
              RaisedButton(
                  child: Text("Click me!"),
                  onPressed: () {
                    Scaffold.of(context).showSnackBar(SnackBar(
                      content: Text("Hi, I am a SnackBar!"),
                    ));
                  }),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 8.0),
                child: Text(
                  "Checkbox:",
                  style: titleStyle,
                ),
              ),
              Checkbox(
                value: checkBox,
                onChanged: onCheckBoxChanged,
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 8.0),
                child: Text(
                  "Radio:",
                  style: titleStyle,
                ),
              ),
              Row(
                children: <Widget>[
                  Radio(
                    value: 1,
                    groupValue: radio,
                    onChanged: onRadioChanged,
                  ),
                  Radio(
                    value: 2,
                    groupValue: radio,
                    onChanged: onRadioChanged,
                  ),
                  Radio(
                    value: 3,
                    groupValue: radio,
                    onChanged: onRadioChanged,
                  ),
                ],
              ),
              Container(
                margin: EdgeInsets.fromLTRB(0.0, 16.0, 0.0, 8.0),
                child: Text(
                  "Image:",
                  style: titleStyle,
                ),
              ),
              Image(
                width: 200.0,
                image: AssetImage("assets/images/timg.jpg"),
              ),
            ]),
      )),
      floatingActionButton:
          FloatingActionButton(child: Icon(Icons.add), onPressed: () {}),
    );
  }
}
