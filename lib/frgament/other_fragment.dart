import 'package:flutter/material.dart';

class OtherFragment extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => OtherFragmentState();
}

class OtherFragmentState extends State<OtherFragment> {
  @override
  Widget build(BuildContext context) {
    return Column(children: <Widget>[
      BackButton(),
      RawMaterialButton(onPressed: null),
      RaisedButton(onPressed: (){}),
      FlutterLogo(),
    ],);
  }
}
