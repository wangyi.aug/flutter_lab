import 'package:flutter/material.dart';

class StarRatingBar extends StatefulWidget {
  final double rating;
  final int starCount;
  final double starSize;
  final Color color;

  StarRatingBar(
      {Key key,
      this.rating = 0.0,
      this.starCount = 5,
      this.starSize,
      this.color});

  @override
  StarRatingBarState createState() => StarRatingBarState(
      rating: this.rating,
      starCount: this.starCount,
      starSize: this.starSize,
      color: this.color);
}

class StarRatingBarState extends State<StarRatingBar> {
  double rating;
  final int starCount;
  final double starSize;
  final Color color;

  StarRatingBarState({this.rating, this.starCount, this.starSize, this.color});

  @override
  Widget build(BuildContext context) {
    return StarRating(
      rating: this.rating,
      starCount: this.starCount,
      starSize: this.starSize,
      color: this.color,
    );
  }
}

class StarRating extends StatelessWidget {
  final int starCount;
  final double rating;
  final double starSize;
  final Color color;

  StarRating(
      {@required this.starCount,
      @required this.starSize,
      this.rating,
      this.color});

  Widget buildStar(BuildContext context, int index) {
    Icon icon;
    if (index >= rating) {
      icon = Icon(
        Icons.star_border,
        color: Theme.of(context).buttonColor,
        size: starSize,
      );
    } else if (index > rating - 1 && index < rating) {
      icon = Icon(
        Icons.star_half,
        color: color ?? Theme.of(context).primaryColor,
        size: starSize,
      );
    } else {
      icon = Icon(
        Icons.star,
        color: color ?? Theme.of(context).primaryColor,
        size: starSize,
      );
    }
    return icon;
  }

  @override
  Widget build(BuildContext context) {
    return Row(
        children:
            List.generate(starCount, (index) => buildStar(context, index)));
  }
}
