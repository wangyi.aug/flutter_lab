import 'package:flutter/material.dart';

import 'package:flutter_sample/page/home.dart';
import 'package:flutter_sample/page/movie_details.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: HomePage(title: 'Flutter Lab'),
      routes: <String, WidgetBuilder>{
        '/movie/details': (BuildContext context) {
          return MovieDetailsPage();
        },
      },
    );
  }
}
