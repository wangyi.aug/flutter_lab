import 'package:flutter/material.dart';

import '../frgament/movie_fragment.dart';
import '../frgament/widget_fragment.dart';
import '../frgament/other_fragment.dart';
import '../frgament/layout_fragment.dart';

class HomePage extends StatefulWidget {
  HomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  HomePageState createState() => HomePageState();
}

class HomePageState extends State<HomePage> {
  PageController pageController;
  int currentPage = 0;
  String title;

  @override
  void initState() {
    super.initState();
    pageController = PageController(initialPage: currentPage);
  }

  @override
  void dispose() {
    pageController.dispose();
    super.dispose();
  }

  String getTitle() {
    switch (currentPage) {
      case 0:
        return "豆瓣电影Top250";
      case 1:
        return "Widget Samples";
      case 2:
        return "Layout Samples";
      case 3:
        return "Other Samples";
      default:
        return "";
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(getTitle())),
      body: PageView(
        controller: pageController,
        children: <Widget>[
          MovieFragment(),
          WidgetFragment(),
          LayoutFragment(),
          OtherFragment(),
        ],
        onPageChanged: (page) {
          setState(() {
            this.currentPage = page;
            this.title = getTitle();
          });
        },
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                Icons.movie,
                color: Colors.black54,
              ),
              title: Text(
                "Movie",
                style: TextStyle(color: Colors.black54),
              ),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.widgets,
                color: Colors.black54,
              ),
              title: Text(
                "Widget",
                style: TextStyle(color: Colors.black54),
              ),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.apps,
                color: Colors.black54,
              ),
              title: Text(
                "Layout",
                style: TextStyle(color: Colors.black54),
              ),
              backgroundColor: Colors.white),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.build,
                color: Colors.black54,
              ),
              title: Text(
                "Other",
                style: TextStyle(color: Colors.black54),
              ),
              backgroundColor: Colors.white),
        ],
        onTap: (index) {
          pageController.animateToPage(index,
              duration: const Duration(milliseconds: 300), curve: Curves.ease);
        },
        currentIndex: currentPage,
      ),
    );
  }
}
