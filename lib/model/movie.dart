class MovieListResponse {
  int count;
  int start;
  int total;
  String title;
  List<Movie> subjects;

  MovieListResponse({
    this.count,
    this.start,
    this.total,
    this.title,
    this.subjects,
  });

  factory MovieListResponse.fromJson(Map<String, dynamic> parsedJson) {
    var subjects = parsedJson['subjects'] as List;
    return MovieListResponse(
        count: parsedJson['count'],
        start: parsedJson['start'],
        total: parsedJson['total'],
        subjects: subjects.map((i) => Movie.fromJson(i)).toList(),
        title: parsedJson['title']);
  }
}

class Movie {
  String title;
  Rating rating;
  List<String> genres;
  List<Cast> casts;
  int collectCount;
  String originalTitle;
  String subtype;
  List<Director> directors;
  String year;
  Images images;
  String alt;
  String id;

  Movie({
    this.title,
    this.rating,
    this.genres,
    this.casts,
    this.collectCount,
    this.originalTitle,
    this.subtype,
    this.directors,
    this.year,
    this.images,
    this.alt,
    this.id,
  });

  factory Movie.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null) {
      return Movie();
    }

    var genres = parsedJson['genres'] as List;
    var casts = parsedJson['casts'] as List;
    var directors = parsedJson['directors'] as List;
    return Movie(
      title: parsedJson['title'],
      rating: Rating.fromJson(parsedJson['rating']),
      genres: genres.map((i) => i as String).toList(),
      casts: casts.map((i) => Cast.fromJson(i)).toList(),
      collectCount: parsedJson['collect_count'],
      originalTitle: parsedJson['original_title'],
      subtype: parsedJson['subtype'],
      directors: directors.map((i) => Director.fromJson(i)).toList(),
      images: Images.fromJson(parsedJson['images']),
      year: parsedJson['year'],
      alt: parsedJson['alt'],
      id: parsedJson['id'],
    );
  }
}

class Rating {
  int max;
  int min;
  double average;
  String stars;

  Rating({
    this.max,
    this.min,
    this.average,
    this.stars,
  });

  factory Rating.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null) {
      return Rating();
    }
    return Rating(
        max: parsedJson['max'],
        min: parsedJson['min'],
        average: parsedJson['average'],
        stars: parsedJson['stars']);
  }
}

class Cast {
  String name;
  String id;
  String alt;
  Images avatars;

  Cast({
    this.name,
    this.id,
    this.alt,
    this.avatars,
  });

  factory Cast.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null) {
      return Cast();
    }
    return Cast(
        name: parsedJson['name'],
        id: parsedJson['id'],
        alt: parsedJson['alt'],
        avatars: Images.fromJson(parsedJson['avatars']));
  }
}

class Images {
  String small;
  String medium;
  String large;

  Images({
    this.small,
    this.medium,
    this.large,
  });

  factory Images.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null) {
      return Images();
    }
    return Images(
        small: parsedJson['small'],
        medium: parsedJson['medium'],
        large: parsedJson['large']);
  }
}

class Director {
  String alt;
  Images avatars;
  String name;
  String id;

  Director({
    this.alt,
    this.avatars,
    this.name,
    this.id,
  });

  factory Director.fromJson(Map<String, dynamic> parsedJson) {
    if (parsedJson == null) {
      return Director();
    }
    return Director(
        alt: parsedJson['alt'],
        avatars: Images.fromJson(parsedJson['avatars']),
        name: parsedJson['name'],
        id: parsedJson['id']);
  }
}
