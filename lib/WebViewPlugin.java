import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry.Registrar;

/**
 * WebviewPlugin
 */
public class WebviewPlugin implements MethodCallHandler {

    private Activity mActivity;
    private WebView mWebView;

    public WebviewPlugin(final Activity activity) {
        mActivity = activity;
        webView = new WebView(activity);
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setCacheMode(WebSettings.LOAD_NO_CACHE);
        webView.getSettings().setLoadsImagesAutomatically(true);
        webView.getSettings().setUseWideViewPort(true);
        webView.getSettings().setAllowFileAccess(true);
    }

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        final MethodChannel channel = new MethodChannel(registrar.messenger(), "webview_plugin");
        channel.setMethodCallHandler(new WebviewPlugin());
    }

    @Override
    public void onMethodCall(MethodCall call, Result result) {
        switch (call.method) {
            case "loadUrl":
                mWebView.setWebViewClient(new MyWebViewClient(mContext, (title, isError)
                        -> titleView.setText(title)));
                mWebView.loadUrl(call.argument("url").toString());
                break;
        }
    }
}
